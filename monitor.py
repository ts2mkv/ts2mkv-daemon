import os
import pyinotify

def start(path, handler):
    class MyEventHandler(pyinotify.ProcessEvent):
        def notify_if_ts(self, fname):
            if fname.endswith('.ts') or fname.endswith('.rec') and os.path.exists(fname):
                handler(fname)
        def process_IN_CLOSE_WRITE(self, event):
            self.notify_if_ts(event.pathname)
        def process_IN_MOVED_TO(self, event):
            self.notify_if_ts(event.pathname)

    wm = pyinotify.WatchManager()
    wm.add_watch(path, pyinotify.IN_CLOSE_WRITE | pyinotify.IN_MOVED_TO)

    notifier = pyinotify.Notifier(wm, MyEventHandler())
    notifier.loop()

if __name__ == '__main__':
    import sys
    def handler(fname):
        print 'handler', fname
    path = sys.argv[1]
    start(path, handler)

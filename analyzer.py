import os
from threading import Timer
import time

from status import Status

notify_copied_file_handler = None
waiting_time = 60 #seconds

def _check_files():
    global timer
    timer = None
    print 'Check last changed times:'
    status = Status()
    for fname in status.get_files_by_status('waiting'):
        if not os.path.exists(fname):
            print 'File deleted:', fname
            status.set_status(fname, 'processed')
            continue
        secs = time.time() - os.stat(fname).st_ctime
        print '    ', fname,
        if secs > waiting_time:
            print '-> process'
            status.set_status(fname, 'copied')
            notify_copied_file_handler(fname)
        else:
            print '-> wait more'
            _start_wait_timer()

timer = None
def _start_wait_timer():
    global timer
    if not timer or not timer.isAlive():
        timer = Timer(waiting_time + 1, _check_files)
        timer.start()
        print 'Timer started'

def add_file_for_analysis(fname):
    status = Status()
    if status.get_status(fname) != 'waiting':
        print 'New file:', fname
    status.set_status(fname, 'waiting')
    _start_wait_timer()

if __name__ == '__main__':
    import signal, sys

    def printcopied(fname):
        print 'File copied:', fname
    notify_copied_file_handler = printcopied

    waiting_time = 10

    open('test/test', 'w').write('test')
    add_file_for_analysis('test/test')
    open('test/test2', 'w').write('test2')
    add_file_for_analysis('test/test2')

    def signal_handler(signal, frame):
        sys.exit(0)
    signal.signal(signal.SIGINT, signal_handler)
    signal.pause()

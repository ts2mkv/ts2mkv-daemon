import sys
from daemon import Daemon
import ts2mkvd

class Ts2mkvDaemon(Daemon):
    def run(self):
        ts2mkvd.main()

def main():
    log = '/var/log/ts2mkvd.log'
    daemon = Ts2mkvDaemon('/tmp/daemon-ts2mkvd.pid', stdout = log, stderr = log)
    if len(sys.argv) == 2:
        if 'start' == sys.argv[1]:
            daemon.start()
        elif 'stop' == sys.argv[1]:
            daemon.stop()
        elif 'restart' == sys.argv[1]:
            daemon.restart()
        else:
            print "Unknown command"
            sys.exit(2)
        sys.exit(0)
    else:
        print "usage: %s start|stop|restart" % sys.argv[0]
        sys.exit(2)

if __name__ == '__main__':
    main()

import os
import time
import config
import shutil

def execute(cmd):
    print 'Executing:', cmd
    os.system(cmd)

def delfile(fname):
    if os.path.exists(fname):
        print 'Removing:', fname
        os.remove(fname)
    else:
        print 'Unable to remove (not found):', fname

def deleteoutdated():
    path = config.path
    deleteafter = config.deleteafter
    for fname in [os.path.abspath(os.path.join(path, fname)) for fname in os.listdir(path) if fname != 'ts2mkvd.db' and os.path.isfile(fname)]:
        secs = time.time() - os.stat(fname).st_ctime
        limit = deleteafter * 24 * 60 * 60
        if secs > limit:
            print 'Outdated file:', fname
            delfile(fname)

def stash(fname):
    shutil.move(fname, os.path.join(config.stashfolder, os.path.split(fname)[1]))

def delete(fname):
    delfile(fname)

def convert(fname):
    olddir = os.getcwd()
    os.chdir(config.outputfolder)
    command = config.command
    execute(command % (fname))
    delfile(fname)
    delfile(os.path.splitext(os.path.split(fname)[1])[0] + '.batchfile')
    delfile(os.path.splitext(os.path.split(fname)[1])[0] + '.X.ini')
    os.chdir(olddir)

def processfile(fname):
    found = False
    func = None
    for rule in config.rules:
        pattern, task = rule
        if pattern.lower() in os.path.split(fname)[1].lower():
            func = eval(task)
            found = True
            break
    if not found:
        func = convert

    func(fname)

if __name__ == '__main__':
    for fname in os.listdir('test'):
        processfile(os.path.join(os.path.abspath('test'), fname))

import os, sys
import thread
import config
import signal
import monitor
from status import Status
import analyzer
import queue

def processfile_thread():
    import processor
    status = Status()
    while True:
        fname = queue.wait_and_pop_from_queue()
        processor.deleteoutdated()
        status.set_status(fname, 'processing')
        processor.processfile(fname)
        status.set_status(fname, 'processed')

def main():
    path = config.path

    analyzer.notify_copied_file_handler = queue.add_to_queue

    for fname in [os.path.abspath(os.path.join(path, fname)) for fname in os.listdir(path) if fname.endswith('.ts') or fname.endswith('.rec')]:
        analyzer.add_file_for_analysis(fname)

    thread.start_new_thread(monitor.start, (path, analyzer.add_file_for_analysis))
    thread.start_new_thread(processfile_thread, ())

    def signal_handler(signal, frame):
        print '\nExiting...'
        if analyzer.timer:
            analyzer.timer.cancel()
        sys.exit(0)

    signal.signal(signal.SIGINT, signal_handler)
    signal.pause()

if __name__ == '__main__':
    main()

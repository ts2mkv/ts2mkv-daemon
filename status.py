import sqlite3

class Status(object):
    def __init__(self):
        self.connection = sqlite3.connect('ts2mkvd.db')
        cursor = self.connection.cursor()
        cursor.execute('CREATE TABLE IF NOT EXISTS status (fname TEXT PRIMARY KEY, status TEXT)')

    def set_status(self, fname, status):
        with self.connection:
            cursor = self.connection.cursor()
            cursor.execute('INSERT OR REPLACE INTO status (fname, status) VALUES ("%s", "%s")' % (fname, status))

    def get_status(self, fname):
        cursor = self.connection.cursor()
        cursor.execute('SELECT status FROM status WHERE fname = "%s"' % (fname))
        row = cursor.fetchone()
        if row:
            return row[0]

    def get_files_by_status(self, status):
        cursor = self.connection.cursor()
        cursor.execute('SELECT fname FROM status WHERE status = "%s"' % (status))
        return [row[0] for row in cursor.fetchall()]

if __name__ == '__main__':
    status = Status()
    print status.get_status('test status key')
    status.set_status('test status key', 'test status value')
    print status.get_status('test status key')

    print status.get_files_by_status('test status value')

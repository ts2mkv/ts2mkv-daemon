import threading

queue = []
mutex = threading.Lock()
semaphore = threading.Semaphore()
semaphore.acquire()

def add_to_queue(fname):
    mutex.acquire()
    queue.append(fname)
    mutex.release()
    semaphore.release()

def wait_and_pop_from_queue():
    semaphore.acquire()
    mutex.acquire()
    fname = queue.pop(0)
    mutex.release()
    return fname

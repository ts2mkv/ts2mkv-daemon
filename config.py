import os
import ConfigParser

def thispath(fname):
    return os.path.join(os.path.split(os.path.abspath(__file__))[0], fname)

config = ConfigParser.ConfigParser()
configfile = thispath('ts2mkvd.conf')
if not os.path.exists(configfile):
    configfile = os.path.join('/etc', configfile)
print 'Config file:', configfile
config.read(configfile)

path = config.get('settings', 'path')
path = os.path.abspath(path)
print '  Path:', path

command = config.get('settings', 'command')
print '  Command:', command

outputfolder = config.get('settings', 'outputfolder')
print '  Output folder:', outputfolder

deleteafter = int(config.get('settings', 'deleteafter'))
print '  Delete after: %d days' % (deleteafter)

stashfolder = config.get('settings', 'stashfolder')
print '  Stash folder:', stashfolder

rules = config.items('rules')
print '  Rules:'
for rule in rules:
    print '    %s: %s' % rule
